# Go Email Parser

A small MIME encoded (A.K.A. EML files) email parser.

This has been used in production for some time, so we decided to extract it as open source.

## Features

- Parse email attachment recursively, as well as structured bounce messages.

## TODO

- comprehensive unit tests
- CI/CD stuff
- Documentation
- Clean up API
- Create eml2json command
