// SPDX-FileCopyrightText: 2023 Lightmeter <hello@lightmeter.io>
//
// SPDX-License-Identifier: Apache License 2.0

package emailparser

import (
	"bufio"
	"bytes"
	"errors"
	"hash/fnv"
	"io"
	"regexp"
	"strings"
	"unicode/utf8"

	ref "gitlab.com/lightmeter/go-pointer-utils"

	"github.com/emersion/go-message/mail"
	"github.com/emersion/go-message/textproto"
)

type Part struct {
	ContentType        string                `json:"content_type"`
	ContentDisposition *string               `json:"content_disposition,omitempty"`
	AsText             *string               `json:"text,omitempty"`
	RawValue           []byte                `json:"raw,omitempty"`
	Parsed             *Email                `json:"parsed,omitempty"`
	Size               *int                  `json:"size,omitempty"`
	Checksum           *uint64               `json:"checksum,omitempty"`
	DeliveryStatus     []map[string][]string `json:"delivery_status,omitempty"`
}

type Email struct {
	Headers map[string][]string `json:"headers,omitempty"`
	Parts   []*Part             `json:"parts,omitempty"`
}

func Headers(p *Email) *mail.Header {
	return ref.Ref(mail.HeaderFromMap(func() map[string][]string {
		if p == nil {
			return nil
		}

		return p.Headers
	}()))
}

func checksum(b []byte) uint64 {
	// fnv-a is a good enough hash function, as we do not  need crypto safe hashes here.
	h := fnv.New64a()
	_, _ = h.Write(b)

	return h.Sum64()
}

// StripOutAttachments removes raw attachments from a message, in order to save
// same space on its JSON encoded representation.
// TODO: maybe this would be better implemented in ParseFromReaderAsPartsWithBody instead,
// in a immutable fashion?
func StripOutAttachments(p *Email) {
	if p == nil {
		return
	}

	for _, part := range p.Parts {
		if part.Parsed != nil {
			// strip the part as well
			StripOutAttachments(part.Parsed)
			continue
		}

		// strip out potentially big parts, such as images, attachments, etc.
		// but keep text based ones, as well as reports, which content-types are prefixed with `text` and `message`
		if !(strings.HasPrefix(part.ContentType, `text/`) || strings.HasPrefix(part.ContentType, `message/`)) || part.AsText == nil {
			part.AsText = nil
			part.RawValue = nil
		}
	}
}

func unparsedPart(b []byte, contentType, contentDisposition string) *Part {
	if !utf8.Valid(b) {
		return &Part{
			RawValue:           b,
			ContentType:        contentType,
			Size:               ref.Ref(len(b)),
			Checksum:           ref.Ref(checksum(b)),
			ContentDisposition: ref.RefIfAny(contentDisposition),
		}
	}

	s := string(b)

	return &Part{
		AsText:             &s,
		ContentType:        contentType,
		Size:               ref.Ref(len(b)),
		Checksum:           ref.Ref(checksum(b)),
		ContentDisposition: &contentDisposition,
	}
}

func drainReader(r io.Reader) {
	_, _ = io.Copy(io.Discard, r)
}

func parsePart(depth int, r io.Reader, contentType, contentDisposition string) *Part {
	var buffer bytes.Buffer

	switch {
	case strings.HasPrefix(contentType, `text/rfc822-headers`):
		rawReader := io.TeeReader(r, &buffer)

		headers, err := textproto.ReadHeader(bufio.NewReader(rawReader))
		if err != nil {
			drainReader(rawReader)
			return unparsedPart(buffer.Bytes(), contentType, contentDisposition)
		}

		return &Part{
			Parsed:             &Email{Headers: sanitizeHeaders(headers.Map())},
			ContentType:        contentType,
			ContentDisposition: ref.RefIfAny(contentDisposition),
		}

	// NOTE: We don't expect more than 10 levels of recursion. It might happen though,
	// if an email contains an attachment which is an email which contains an attachment which is an attachment that contains.... and so on...
	// Those are very edge cases and I don't ever believe we'll need to handle them!
	// It also prevents parsing some more complex cases.
	case (strings.HasPrefix(contentType, `message/rfc822`) || strings.HasPrefix(contentType, `message/feedback-report`)) && depth < 10:
		rawReader := io.TeeReader(r, &buffer)

		parsedBody, err := parseFromReaderAsPartsWithBodyWithDepth(depth, rawReader)
		if err != nil {
			drainReader(rawReader)
			return unparsedPart(buffer.Bytes(), contentType, contentDisposition)
		}

		return &Part{Parsed: parsedBody, ContentType: contentType}

	case strings.HasPrefix(contentType, `message/delivery-status`):
		// The delivery-status is essentially composed by groups of headers like structures,
		// separated by an empty line.
		// we should therefore represent it as something like []map[string][]string.
		// But, for now, we just ignore it and get the raw content.
		// spec at https://datatracker.ietf.org/doc/html/rfc3464#section-2
		rawReader := io.TeeReader(r, &buffer)

		part, err := buildDeliveryStatus(rawReader, contentType, contentDisposition)
		if err != nil {
			drainReader(rawReader)
			return unparsedPart(buffer.Bytes(), contentType, contentDisposition)
		}

		return part

	default:
		// if this copy fails, use whatever info we're able to read.
		// In practice this should fail only if r.Read() fails, which we don't care here.
		_, _ = io.Copy(&buffer, r)

		return unparsedPart(buffer.Bytes(), contentType, contentDisposition)
	}
}

func sanitizeHeaders(h map[string][]string) map[string][]string {
	r := make(map[string][]string, len(h))

	fromMap := mail.HeaderFromMap(h)

	for k, v := range h {
		if len(v) != 1 {
			r[k] = v
			continue
		}

		if t, err := fromMap.Text(k); err == nil {
			r[k] = []string{t}
			continue
		}

		r[k] = v
	}

	return r
}

func ParseFromReader(r io.Reader) (*Email, error) {
	return parseFromReaderAsPartsWithBodyWithDepth(0, r)
}

func parseFromReaderAsPartsWithBodyWithDepth(depth int, r io.Reader) (*Email, error) {
	mr, err := mail.CreateReader(r)
	if err != nil {
		return nil, err
	}

	result := &Email{
		Headers: sanitizeHeaders(mr.Header.Map()),
	}

	// Process each message's part
	for {
		p, err := mr.NextPart()
		if err != nil && errors.Is(err, io.EOF) {
			break
		}

		if err != nil {
			return nil, err
		}

		// A part without headers is not a valid part, so we just stop here.
		if p.Header == nil {
			break
		}

		parsedPart := parsePart(depth+1, p.Body, p.Header.Get(`Content-Type`), p.Header.Get(`Content-Disposition`))

		result.Parts = append(result.Parts, parsedPart)
	}

	return result, nil
}

func buildDeliveryStatus(r io.Reader, contentType, contentDisposition string) (*Part, error) {
	v, err := io.ReadAll(r)

	if err != nil {
		return nil, err
	}

	var d []map[string][]string = nil

	rawComponents := bytes.Split(v, []byte("\r\n\r\n"))

	// each component is a bunch of records, with a "key", colon and then some content.
	// the content might spawn over multiple lines, so it's not a standard rfc822 header!
	for _, c := range rawComponents {
		if entries := parseDeliveryStatusComponentEntries(c); len(entries) > 0 {
			d = append(d, entries)
		}
	}

	return &Part{
		ContentType:        contentType,
		DeliveryStatus:     d,
		ContentDisposition: ref.RefIfAny(contentDisposition),
	}, nil
}

var deliveryStatusComponentKeyPattern = regexp.MustCompile(`^([A-Z][a-zA-Z0-9-]*): (.*)$`)

// multiple entries in the form `Key: Value`, where the value can spawn over multiple lines
func parseDeliveryStatusComponentEntries(c []byte) map[string][]string {
	m := map[string][]string{}
	key := make([]byte, 0, len(c))
	acc := make([]byte, 0, len(c))
	scanner := bufio.NewScanner(bytes.NewReader(c))

	tryToConsume := func() {
		if len(key) > 0 && len(acc) > 0 {
			m[string(key)] = []string{string(acc)}
			key = nil
			acc = nil
		}
	}

	for scanner.Scan() {
		line := scanner.Bytes()

		matches := deliveryStatusComponentKeyPattern.FindSubmatch(line)

		if len(matches) == 0 {
			acc = append(acc, line...)
			continue
		}

		// found something from the previous entry.
		// consume it
		tryToConsume()

		// accumulate found values
		key = append(key, matches[1]...)
		acc = append(acc, matches[2]...)
	}

	tryToConsume()

	return m
}
