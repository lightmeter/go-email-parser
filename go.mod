module gitlab.com/lightmeter/go-email-parser

go 1.20

require (
	github.com/emersion/go-message v0.17.0
	gitlab.com/lightmeter/go-pointer-utils v0.0.0-20231211170802-282f9399a06d
)

require github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
