package main

import (
	"encoding/json"
	"os"

	emailparser "gitlab.com/lightmeter/go-email-parser"
)

func main() {
	parsed, err := emailparser.ParseFromReader(os.Stdin)
	if err != nil {
		panic(err)
	}

	err = json.NewEncoder(os.Stdout).Encode(parsed)
	if err != nil {
		panic(err)
	}
}
